/* SPDX-FileCopyrightText: 2024-2025 - Sébastien Wilmet
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "gfls-config.h"
#include "gfls-error.h"
#include <glib/gi18n-lib.h>

GQuark
gfls_loader_error_quark (void)
{
	static GQuark quark = 0;

	if (G_UNLIKELY (quark == 0))
	{
		quark = g_quark_from_static_string ("gfls-loader-error");
	}

	return quark;
}

GError *
_gfls_loader_error_too_big_file_size (void)
{
	return g_error_new_literal (GFLS_LOADER_ERROR,
				    GFLS_LOADER_ERROR_TOO_BIG,
				    _("The size of the file is too big."));
}

GError *
_gfls_loader_error_too_big_read (void)
{
	return g_error_new_literal (GFLS_LOADER_ERROR,
				    GFLS_LOADER_ERROR_TOO_BIG,
				    _("Limit on the number of bytes to read reached."));
}

GError *
_gfls_loader_error_not_utf8 (void)
{
	return g_error_new_literal (GFLS_LOADER_ERROR,
				    GFLS_LOADER_ERROR_NOT_UTF8,
				    _("The content is not encoded in UTF-8."));
}

GError *
_gfls_loader_error_has_very_long_line (void)
{
	return g_error_new_literal (GFLS_LOADER_ERROR,
				    GFLS_LOADER_ERROR_HAS_VERY_LONG_LINE,
				    _("The content contains a very long line."));
}
