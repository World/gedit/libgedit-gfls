/* SPDX-FileCopyrightText: 2023-2024 - Sébastien Wilmet
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "gfls-utf8.h"

/**
 * SECTION:gfls-utf8
 * @Title: GflsUtf8
 * @Short_description: Functions for UTF-8 strings
 *
 * Additional functions for UTF-8 strings, to complement what is provided by
 * GLib.
 */

/**
 * gfls_utf8_find_very_long_line:
 * @str: a UTF-8 nul-terminated string.
 * @max_n_bytes_per_line: the maximum number of bytes per line, without counting
 *   the newline character(s).
 *
 * Finds if a line in @str exceeds @max_n_bytes_per_line.
 *
 * For performance reasons, @str is not checked whether it is a valid UTF-8
 * string. So you must call for example g_utf8_validate() beforehand.
 *
 * Returns: a pointer to the beginning of the first very long line found, or
 *   %NULL if not found.
 * Since: 0.3
 */
const gchar *
gfls_utf8_find_very_long_line (const gchar *str,
			       guint        max_n_bytes_per_line)
{
	guint cur_pos;
	guint line_length = 0;

	g_return_val_if_fail (str != NULL, NULL);

	for (cur_pos = 0; str[cur_pos] != '\0'; cur_pos++)
	{
		gchar ch = str[cur_pos];

		if (ch == '\n' || ch == '\r')
		{
			line_length = 0;
		}
		else
		{
			line_length++;

			if (line_length > max_n_bytes_per_line)
			{
				guint line_start_pos = cur_pos - line_length + 1;
				return str + line_start_pos;
			}
		}
	}

	return NULL;
}
